# How to run project in local environment (react-form-hook)

##### 1. open project file

- open terminal in project file

##### 2. install package json

- ### `npm install`

##### 3. run project

- ### `npm run start`

##### 4. open in web browser ex. chrome eadge safari firefox

- Open [http://localhost:3000](http://localhost:3000) to view it in the browser

##### credit by Kongkom Sumlee
