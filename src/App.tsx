import React from 'react'
import SignupForm from './components/SignUpForm'
import './App.css'

const App: React.FC = () => {
  return (
    <div className='mt-5'>
      <h1>Signup Form</h1>
      <SignupForm />
    </div>
  )
}

export default App
