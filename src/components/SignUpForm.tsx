import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { FormData } from './../interfaces/FormData'
import './SignupForm.css'

const SignUpForm: React.FC = () => {
    const { register, handleSubmit } = useForm<FormData>();
    const [selectedInterests, setSelectedInterests] = useState<string[]>([])
    const [showSuccessAlert, setShowSuccessAlert] = useState(false)

    const onSubmit = (data: FormData) => {
        data.interests = selectedInterests
        console.log(data)

        setShowSuccessAlert(true)

        setTimeout(() => {
            setShowSuccessAlert(false)
        }, 3000)
    }

    const handleInterestChange = (value: string) => {
        setSelectedInterests((previousInterests) => {
            if (previousInterests.includes(value)) {
                return previousInterests.filter((interest) => interest !== value)
            } else {
                return [...previousInterests, value]
            }
        })
    }

    return (
        
        <div className="container mt-5">
            {showSuccessAlert && (
                <div className="alert alert-success" role="alert">
                Successfully
                </div>
            )}
            <br />
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                    <label className="form-label">Firstname</label>
                    <input className="form-control" {...register('firstname')} />
                </div>
                <div className="mb-3">
                    <label className='form-label'>Lastname</label>
                    <input className='form-control' {...register('lastname')} />
                </div>
                <div className="mb-3">
                    <label className='form-label'>Email</label>
                    <input className='form-control' type="email" {...register('email')} />
                </div>
                <div className="mb-3">
                    <label className='form-label'>Username</label>
                    <input className='form-control' {...register('username')} />
                </div>
                <div className="mb-3">
                    <label className='form-label'>Password</label>
                    <input className='form-control' type="password" {...register('password')} />
                </div>

                <div className='mb-3'>
                    <label className='interestLabel'>Interest</label>

                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type="checkbox"
                            onChange={() => handleInterestChange('React')}
                            checked={selectedInterests.includes('React')}
                        />
                        <label className='form-check-label'>React</label>
                    </div>
                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type="checkbox"
                            onChange={() => handleInterestChange('Next.js')}
                            checked={selectedInterests.includes('Next.js')}
                        />
                        <label className='form-check-label'>Next.js</label>
                    </div>
                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type="checkbox"
                            onChange={() => handleInterestChange('Laravel')}
                            checked={selectedInterests.includes('Laravel')}
                        />
                        <label className='form-check-label'>Laravel</label>
                    </div>
                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type="checkbox"
                            onChange={() => handleInterestChange('GraphQL')}
                            checked={selectedInterests.includes('GraphQL')}
                        />
                        <label className='form-check-label'>GraphQL</label>
                    </div>
                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type="checkbox"
                            onChange={() => handleInterestChange('Nest.js')}
                            checked={selectedInterests.includes('Nest.js')}
                        />
                        <label className='form-check-label'>Nest.js</label>
                    </div>
                </div>
                <div >
                    <button type="submit" className="btn btn-primary btn-lg btn-block custom-button">Submit</button>
                </div>
            </form>
        </div>
    )
}

export default SignUpForm
